import { productList } from "@/lib/constant";
import { useState } from "react";

const useProduct = () => {
  const [search, setSearch] = useState("");
  const [currentPage, setCurrentPage] = useState(1);
  const dataPerPage = 5; // Jumlah produk yang ditampilkan

  // menghitung total harga semua produk
  const totalPrice = productList.reduce(
    (acc, product) => acc + product.price,
    0
  );

  // Variabel yang berisi produk dengan harga diatas 100
  const productAbove100 = (param, type = ">100") => {
    const typeMap = {
      ">100": (product) => product.price > 100,
    };
    return [...param].filter(typeMap[type]);
  };

  // Rata-rata harga produk
  const averagePrice = (param) => {
    const average = (
      param.reduce((acc, product) => acc + product.price, 0) / param.length
    ).toFixed(2);
    return parseFloat(average);
  };

  //Urutkan produk dari termurah ke termahal
  const sortProduct = (param, type = "asc") => {
    const typeMap = {
      asc: (a, b) => a.price - b.price,
      desc: (a, b) => b.price - a.price,
    };
    return [...param].sort(typeMap[type]);
  };

  // Jumlah Produk dalam setiap kategori
  const calculateProductCategory = (param) => {
    const categoryCount = param.reduce((acc, product) => {
      acc[product.category] = (acc[product.category] || 0) + 1;
      return acc;
    }, {});
    return Object.keys(categoryCount).map((category) => ({
      name: category,
      value: categoryCount[category],
    }));
  };

  // Mengganti nama produk sesuai harga
  const updateProductName = [...productList].map((product) => {
    const newProduct = { ...product };
    if (product.price < 50) {
      newProduct.name = "Produk Hemat";
    } else if (product.price > 200) {
      newProduct.name = "Produk Premium";
    }
    return newProduct;
  });

  //fungsi ini digunakan u/ memfilter data product berdasarkan nama, dan memnentukan produk yan gingin ditampilkan ada berapa, digunakan untuk searchhing
  const filteredData = (data) => {
    const filterData = data.filter((item) => {
      return search.toLowerCase() === ""
        ? item
        : item.name.toLowerCase().includes(search.toLowerCase());
    });
    const paginatedData = filterData.slice(
      (currentPage - 1) * dataPerPage,
      currentPage * dataPerPage
    );
    return {
      paginatedData,
      totalFilteredItems: filterData.length,
    };
  };

  //function untuk memformat uang menjadi format dollar
  const formatMoney = (price) => {
    return price.toLocaleString("en-US", {
      style: "currency",
      currency: "USD",
    });
  };

  return {
    productList,
    currentPage,
    setCurrentPage,
    dataPerPage,
    filteredData,
    setSearch,
    totalPrice,
    productAbove100,
    averagePrice,
    calculateProductCategory,
    updateProductName,
    filteredData,
    formatMoney,
    sortProduct,
  };
};

export default useProduct;
