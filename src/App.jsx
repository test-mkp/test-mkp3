import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import { ThemeProvider } from "@/components/ui/theme-provider";
import Layout from "./components/layout/Layout";
import ExpensivePrice from "./pages/ExpensivePrice";
import SortedProduct from "./pages/SortedProduct";
import ProductChart from "./pages/ProductChart";
import ProductList from "./pages/ProductList";
import Dashboard from "./pages/Dashboard";
import UpdateProductName from "./pages/UpdateProductName";
import CombinedFeatures from "./pages/CombinedFeatures";

function App() {
  return (
    <ThemeProvider defaultTheme="dark" storageKey="vite-ui-theme">
      <Router>
        <Routes>
          <Route element={<Layout />}>
            <Route path="/" element={<Dashboard />} />
            <Route path="/combined-features" element={<CombinedFeatures />} />
            <Route path="/product-list" element={<ProductList />} />
            <Route path="/expensive-price" element={<ExpensivePrice />} />
            <Route path="/sorted-product" element={<SortedProduct />} />
            <Route path="/product-chart" element={<ProductChart />} />
            <Route
              path="/update-product-name"
              element={<UpdateProductName />}
            />
          </Route>
        </Routes>
      </Router>
    </ThemeProvider>
  );
}

export default App;
