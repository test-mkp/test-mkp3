import React from "react";
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableHeader,
  TableRow,
} from "@/components/ui/table";
import useProduct from "@/hooks/useProduct";

const TableComp = ({ data, currentPage }) => {
  const { formatMoney, dataPerPage } = useProduct();

  const startIndex = (currentPage - 1) * dataPerPage;

  return (
    <div>
      <Table className="bg-[#21222D] rounded-md ">
        <TableHeader>
          <TableRow>
            <TableHead className="w-10">No</TableHead>
            <TableHead className="">Name</TableHead>
            <TableHead className=""> Price</TableHead>
            <TableHead className=""> Category</TableHead>
          </TableRow>
        </TableHeader>

        <TableBody>
          {data.length > 0 ? (
            data.map((item, index) => {
              const { id, name, price, category } = item;
              return (
                <TableRow key={id} className="text-white">
                  <TableCell className="font-medium">
                    {startIndex + index + 1}
                  </TableCell>
                  <TableCell>{name}</TableCell>
                  <TableCell>{formatMoney(price)}</TableCell>
                  <TableCell>{category}</TableCell>
                </TableRow>
              );
            })
          ) : (
            <TableRow>
              <TableCell colSpan="4" className="text-center text-white">
                Produk tidak ditemukan
              </TableCell>
            </TableRow>
          )}
        </TableBody>
      </Table>
    </div>
  );
};

export default TableComp;
