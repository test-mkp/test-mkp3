import React from "react";
import useProduct from "@/hooks/useProduct";

const TableSmallComp = ({ data, currentPage }) => {
  const { formatMoney, dataPerPage } = useProduct();
  return (
    <div>
      {data.length > 0 ? (
        data.map((item, index) => {
          const { id, name, price, category } = item;

          const startIndex = (currentPage - 1) * dataPerPage;

          return (
            <div
              key={id}
              className="bg-[#21222D] flex flex-col rounded-md p-4 mb-2 gap-2 shadow-sm "
            >
              <div className="flex justify-between ">
                <p>No</p> <p> {startIndex + index + 1}</p>
              </div>
              <div className="flex justify-between">
                <p>Name</p> <p>{name}</p>
              </div>
              <div className="flex justify-between">
                <p>Price</p> <p>{formatMoney(price)}</p>
              </div>
              <div className="flex justify-between">
                <p>Category</p> <p>{category}</p>
              </div>
            </div>
          );
        })
      ) : (
        <div>
          <p className="text-center text-white">Produk tidak ditemukan</p>
        </div>
      )}
    </div>
  );
};

export default TableSmallComp;
