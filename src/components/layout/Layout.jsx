import React, { useState } from "react";
import { Outlet } from "react-router";
import Sidebar from "./Sidebar";
import Header from "./Header";

const Layout = () => {
  const [toggleCollapse, setToggleCollapse] = useState(false);

  return (
    <div className="flex">
      <Sidebar
        toggleCollapse={toggleCollapse}
        setToggleCollapse={setToggleCollapse}
      />{" "}
      {/* Sidebar tetap di tempat */}
      <div className="w-full">
        <Header />
        <div
          className={`mt-14  pr-4  ${
            toggleCollapse
              ? "pl-[50px]"
              : window.innerWidth <= 970
              ? "pl-[50px]"
              : "pl-[220px]"
          }`}
        >
          {" "}
          <Outlet /> {/* Tempat konten berubah */}
        </div>
      </div>
    </div>
  );
};

export default Layout;
