import React from "react";

const Header = () => {
  return (
    <header className="fixed z-10  w-full pl-[200px] px-6">
      <div className="flex items-center h-14">
        <div className="mr-3 font-semibold text-sm flex  items-center gap-4">
          <span></span>
        </div>
      </div>
    </header>
  );
};

export default Header;
