import React, { useEffect } from "react";
import SideList from "./SideList";
import { BiMenu } from "react-icons/bi";

const Sidebar = ({ toggleCollapse, setToggleCollapse }) => {
  // fungsi untuk mengatur sidebar terbuka/tertutup di ukuran layar <=970
  const handleResize = () => {
    if (window.innerWidth <= 970) {
      setToggleCollapse(true);
    } else {
      setToggleCollapse(false);
    }
  };
  useEffect(() => {
    // Set initial state based on window size
    handleResize();
    window.addEventListener("resize", handleResize);
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);
  return (
    <div
      className={`fixed text-[#87888C] z-50 h-full transition duration-300 ease-in-out border border-r-[#2C2D33] ${
        toggleCollapse ? "w-12" : "w-[220px]"
      } bg-[#010317]`}
    >
      <div className="flex items-center text-center py-3 px-4 text-xl font-semibold">
        {toggleCollapse ? "" : <h3 className="ml-1 ">Product List</h3>}

        <button
          onClick={() => setToggleCollapse(!toggleCollapse)}
          className={`px-2 relative text-black bg-[#54a8c7] rounded-sm py-2 ml-3 hover:bg-blue-500 transition duration-300 ease-in-out ${
            toggleCollapse ? "right-6" : "left-24"
          }`}
        >
          <BiMenu />
        </button>
      </div>
      <div className="mt-4">
        <SideList toggleCollapse={toggleCollapse} />
      </div>
    </div>
  );
};

export default Sidebar;
