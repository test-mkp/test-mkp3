import React from "react";
import { RiHomeFill } from "react-icons/ri";
import {
  FaSortAmountDown,
  FaChartBar,
  FaList,
  FaExchangeAlt,
} from "react-icons/fa";
import { FaArrowTrendUp } from "react-icons/fa6";
import { Link, useLocation } from "react-router-dom";
import { LuCombine } from "react-icons/lu";

const SideList = ({ toggleCollapse }) => {
  const sideLList = [
    { title: "Dashboard", path: "/", icon: <RiHomeFill /> },
    {
      title: "Gabungan Fitur",
      path: "/combined-features",
      icon: <LuCombine />,
    },
    { title: "Daftar Produk", path: "/product-list", icon: <FaList /> },
    {
      title: "Harga Produk > $100",
      path: "/expensive-price",
      icon: <FaArrowTrendUp />,
    },
    {
      title: "Produk Diurutkan",
      path: "/sorted-product",
      icon: <FaSortAmountDown />,
    },
    { title: "Product Chart", path: "/product-chart", icon: <FaChartBar /> },
    {
      title: "Ganti Nama Product",
      path: "/update-product-name",
      icon: <FaExchangeAlt />,
    },
  ];

  const location = useLocation();
  return (
    <div>
      <ul className="flex flex-col gap-3 text-sm">
        {sideLList?.map((item, sidebar) => {
          // membuat variabel isActive untuk menghandle ketika path yang sedang aktif maka bisa kita atur background dan lainya
          const isActive = location.pathname === item.path;
          return (
            <Link to={item.path} key={sidebar}>
              <li
                className={`flex items-center gap-2  px-2 py-2 rounded-md ${
                  isActive && !toggleCollapse ? "bg-[#54a8c7] text-black" : ""
                }  ${
                  toggleCollapse
                    ? isActive
                      ? " justify-center  text-[#54a8c7] rounded-none pl-[9px] border-r-2 border-r-[#54a8c7]"
                      : " justify-center "
                    : "mx-5"
                }`}
              >
                {toggleCollapse ? (
                  <span>{item.icon}</span>
                ) : (
                  <div className="flex items-center gap-2">
                    <span>{item.icon}</span>
                    <span>{item.title}</span>
                  </div>
                )}
              </li>
            </Link>
          );
        })}
      </ul>
    </div>
  );
};

export default SideList;
