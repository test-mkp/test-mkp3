import React, { useState } from "react";
import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuItem,
  DropdownMenuTrigger,
} from "@/components/ui/dropdown-menu";
import { FaCaretDown, FaCaretUp } from "react-icons/fa";

const Dropdown = ({ label, item, onSelect, setName, name }) => {
  return (
    <div>
      <DropdownMenu>
        <div className="flex justify-between items-center ">
          <p className="mr-4">{label}</p>
          <DropdownMenuTrigger className="flex justify-between items-center px-3 w-40 bg-[#2B2B36] py-2 rounded-sm gap-6">
            <p className="w-40 text-sm text-start">{name}</p>
            <span className="text-sm ">
              <FaCaretUp />
              <FaCaretDown />
            </span>
          </DropdownMenuTrigger>
        </div>
        <DropdownMenuContent>
          {item.map((item) => (
            <DropdownMenuItem key={item.value} className="w-36">
              <button
                onClick={() => {
                  onSelect(item.value);
                  setName(item.label);
                }}
                className="w-full text-start"
              >
                {item.label}
              </button>
            </DropdownMenuItem>
          ))}
        </DropdownMenuContent>
      </DropdownMenu>
    </div>
  );
};

export default Dropdown;
