import React from "react";
import { GrPrevious, GrNext } from "react-icons/gr";

const Pagination = ({
  currentPage,
  setCurrentPage,
  totalItems,
  itemsPerPage,
}) => {
  const totalPages = Math.ceil(totalItems / itemsPerPage);

  const handlePreviousClick = () => {
    if (currentPage > 1) {
      setCurrentPage(currentPage - 1);
    }
  };

  const handleNextClick = () => {
    if (currentPage < totalPages) {
      setCurrentPage(currentPage + 1);
    }
  };

  return (
    <div className="flex justify-center md:justify-end items-center text-sm mt-4 gap-4 text-black">
      <div>
        <button
          onClick={handlePreviousClick}
          className="w-20 p-2 bg-[#54a8c7] hover:bg-blue-500 rounded-md flex justify-center items-center"
        >
          <GrPrevious /> Previous
        </button>
      </div>
      <div className="page-number text-white">
        {totalPages === 0 ? "" : `${currentPage} of ${totalPages}`}
      </div>
      <div>
        <button
          onClick={handleNextClick}
          className="w-20 p-2 bg-[#54a8c7] hover:bg-blue-500 rounded-md flex justify-around items-center"
        >
          <span>Next</span>
          <span>
            <GrNext />
          </span>
        </button>
      </div>
    </div>
  );
};

export default Pagination;
