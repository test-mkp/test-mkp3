import React from "react";

const InputSearch = ({ setSearch, setCurrentPage }) => {
  return (
    <input
      type="text"
      placeholder="search"
      className="bg-[#2B2B36] w-full md:w-52 p-2 px-3 rounded-md mt-4"
      onChange={(e) => {
        setSearch(e.target.value);
        setCurrentPage(1);
      }}
    />
  );
};

export default InputSearch;
