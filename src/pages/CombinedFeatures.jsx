import InputSearch from "@/components/common/InputSearch";
import Pagination from "@/components/common/Pagination";
import TableComp from "@/components/layout/TableComp";
import TableSmallComp from "@/components/layout/TableSmallComp";
import useProduct from "@/hooks/useProduct";
import React, { useState } from "react";
import Dropdown from "@/components/common/Dropdown";
const sortItems = [
  { label: "None", value: "None" },
  { label: "Price (Asc)", value: "asc" },
  { label: "Price (Desc)", value: "desc" },
];
const filterItem = [
  { label: "All", value: "All" },
  { label: "Harga diatas 100", value: ">100" },
];
const CombinedFeatures = () => {
  const [sortName, setSortName] = useState("None");
  const [filterName, setFilterName] = useState("All");
  const {
    sortProduct,
    setSearch,
    currentPage,
    setCurrentPage,
    calculateProductCategory,
    dataPerPage,
    productAbove100,
    updateProductName,
    totalPrice,
    averagePrice,
    filteredData,
    formatMoney,
  } = useProduct();
  const [dataProduct, setDataProduct] = useState(updateProductName);

  const { paginatedData, totalFilteredItems } = filteredData(dataProduct);
  const totalProduckByCategory = calculateProductCategory(updateProductName);

  return (
    <div className="mx-6 my-20">
      <h1 className="text-xl font-bold mb-4">Daftar Produk</h1>
      <div className="flex flex-col-reverse md:flex-row justify-between  w-full pb-1">
        {/* menampilkan InputSearch untuk searching */}
        <div className=" pb-3">
          <InputSearch setSearch={setSearch} setCurrentPage={setCurrentPage} />
        </div>
        <div className="flex flex-col w-full md:items-center justify-between md:justify-end md:flex-row gap-2 ">
          {/* menampilkan dropdown untuk mengurutkan data berdasarkan harga terendah/tertinggi */}
          <Dropdown
            label="Sorted by"
            item={sortItems}
            onSelect={(value) =>
              setDataProduct((prev) =>
                value === "None" ? updateProductName : sortProduct(prev, value)
              )
            }
            setName={setSortName}
            name={sortName}
          />
          {/* menampilkan dropdown untuk memfilter data  yang memiliki harga di atas 100 */}
          <Dropdown
            label="Filter by"
            item={filterItem}
            onSelect={(value) =>
              setDataProduct((prev) =>
                value === "All"
                  ? updateProductName
                  : productAbove100(prev, value)
              )
            }
            setName={setFilterName}
            name={filterName}
          />
        </div>
      </div>

      {/* menampilkan data product */}
      <div className="hidden md:block">
        <TableComp data={paginatedData} currentPage={currentPage} />
      </div>
      <div className="block md:hidden">
        <TableSmallComp data={paginatedData} currentPage={currentPage} />
      </div>
      {/* menampilkan info total price, average price, total per kategori & pagination */}
      <div className="flex flex-col-reverse md:flex-row md:justify-between ">
        <div className="flex gap-4 text-sm flex-col items-center   md:flex-row font-bold text-[#87888C]  pt-6">
          <p className="flex  gap-2">
            <span>Total Price :</span> <span>{formatMoney(totalPrice)}</span>
          </p>
          <p className="flwx gap-2">
            <span>Average Price : </span>
            <span>{formatMoney(averagePrice(updateProductName))}</span>
          </p>
          {totalProduckByCategory.map((item, index) => (
            <div key={index} className="flex gap-2">
              <span>{item.name} :</span> <span>{item.value}</span>
            </div>
          ))}
        </div>
        <Pagination
          currentPage={currentPage}
          setCurrentPage={setCurrentPage}
          totalItems={totalFilteredItems}
          itemsPerPage={dataPerPage}
        />
      </div>
    </div>
  );
};

export default CombinedFeatures;
