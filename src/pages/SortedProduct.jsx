import InputSearch from "@/components/common/InputSearch";
import Pagination from "@/components/common/Pagination";
import TableComp from "@/components/layout/TableComp";
import TableSmallComp from "@/components/layout/TableSmallComp";
import useProduct from "@/hooks/useProduct";
import React from "react";

const SortedProduct = () => {
  const {
    productList,
    currentPage,
    setCurrentPage,
    dataPerPage,
    setSearch,
    sortProduct,
    filteredData,
  } = useProduct();

  const { paginatedData, totalFilteredItems } = filteredData(
    sortProduct(productList)
  );

  return (
    <div className="mx-6 my-20">
      <h1 className="text-xl font-bold mb-4">Produk Diurutkan</h1>
      <div className="flex flex-col-reverse md:flex-row justify-between  pb-3">
        {/* menampilkan InputSearch untuk fitur searching */}
        <InputSearch setSearch={setSearch} setCurrentPage={setCurrentPage} />
      </div>
      {/* menampilkan table produk yang sudah di urutkan berdasarkan harga */}
      <div className="hidden md:block">
        <TableComp data={paginatedData} currentPage={currentPage} />
      </div>
      <div className="block md:hidden">
        <TableSmallComp data={paginatedData} currentPage={currentPage} />
      </div>
      {/* menampilkan  pagination */}
      <Pagination
        currentPage={currentPage}
        setCurrentPage={setCurrentPage}
        totalItems={totalFilteredItems}
        itemsPerPage={dataPerPage}
      />
    </div>
  );
};

export default SortedProduct;
