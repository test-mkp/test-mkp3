import InputSearch from "@/components/common/InputSearch";
import Pagination from "@/components/common/Pagination";
import TableComp from "@/components/layout/TableComp";
import TableSmallComp from "@/components/layout/TableSmallComp";
import useProduct from "@/hooks/useProduct";

import React from "react";

const ExpensivePrice = () => {
  const {
    productList,
    currentPage,
    setCurrentPage,
    dataPerPage,
    productAbove100,
    setSearch,
    filteredData,
    averagePrice,
    formatMoney,
  } = useProduct();

  const productFilter = productAbove100(productList);

  const { paginatedData, totalFilteredItems } = filteredData(productFilter);

  return (
    <div className="mx-6 my-20">
      <h1 className="text-xl font-bold mb-4">Harga Produk {">"} $100</h1>
      {/* menampilkan InputSearch untuk searching */}
      <div className="flex flex-col-reverse md:flex-row justify-between  pb-3">
        <InputSearch setSearch={setSearch} setCurrentPage={setCurrentPage} />
      </div>
      {/* menampilkan table harga yang memiliki harga diatas 100 */}
      <div className="hidden md:block">
        <TableComp data={paginatedData} currentPage={currentPage} />
      </div>
      <div className="block md:hidden">
        <TableSmallComp data={paginatedData} currentPage={currentPage} />
      </div>
      {/* menampilkan info average price dan juga pagination */}
      <div className="flex flex-col-reverse md:flex-row md:justify-between ">
        <div className="flex text-sm flex-col md:flex-row mx-auto md:mx-0 font-bold text-[#87888C] gap-3 pt-6">
          <p>Average Price : {formatMoney(averagePrice(productFilter))}</p>
        </div>
        <Pagination
          currentPage={currentPage}
          setCurrentPage={setCurrentPage}
          totalItems={totalFilteredItems}
          itemsPerPage={dataPerPage}
        />
      </div>
    </div>
  );
};

export default ExpensivePrice;
