import InputSearch from "@/components/common/InputSearch";
import Pagination from "@/components/common/Pagination";
import TableComp from "@/components/layout/TableComp";
import TableSmallComp from "@/components/layout/TableSmallComp";
import useProduct from "@/hooks/useProduct";
import React from "react";

const ProductList = () => {
  const {
    productList,
    setSearch,
    currentPage,
    setCurrentPage,
    dataPerPage,
    totalPrice,
    averagePrice,
    filteredData,
    formatMoney,
  } = useProduct();

  const { paginatedData, totalFilteredItems } = filteredData(productList);

  return (
    <div className="mx-6 my-20">
      <h1 className="text-xl font-bold mb-4">Daftar Produk</h1>
      {/* menampilkan InputSearch unutk search data product */}
      <div className="flex flex-col-reverse md:flex-row justify-between  pb-3">
        <InputSearch setSearch={setSearch} setCurrentPage={setCurrentPage} />
      </div>
      {/* menampilkan table daftar produk keseluruhan */}
      <div className="hidden md:block">
        <TableComp data={paginatedData} currentPage={currentPage} />
      </div>
      <div className="block md:hidden">
        <TableSmallComp data={paginatedData} currentPage={currentPage} />
      </div>
      {/* menampilkan info tentang total price & average price */}
      <div className="flex flex-col-reverse md:flex-row md:justify-between ">
        <div className="flex text-sm flex-col md:flex-row mx-auto md:mx-0 font-bold text-[#87888C] gap-3 pt-6">
          <p>Tota Price : {formatMoney(totalPrice)}</p>
          <p>Average Price : {formatMoney(averagePrice(productList))}</p>
        </div>
        {/* menampilkan pagination */}
        <Pagination
          currentPage={currentPage}
          setCurrentPage={setCurrentPage}
          totalItems={totalFilteredItems}
          itemsPerPage={dataPerPage}
        />
      </div>
    </div>
  );
};

export default ProductList;
