import React from "react";
import { Link } from "react-router-dom";
import TableSmallComp from "@/components/layout/TableSmallComp";
import TableComp from "@/components/layout/TableComp";
import useProduct from "@/hooks/useProduct";
import { TbSum, TbBorderHorizontal } from "react-icons/tb";
import { MdOutlineProductionQuantityLimits } from "react-icons/md";

const Dashboard = () => {
  const {
    productList,
    currentPage,
    totalPrice,
    calculateProductCategory,
    productAbove100,
    averagePrice,
    updateProductName,
    formatMoney,
  } = useProduct();
  const data = calculateProductCategory(productList);
  const filterProduct = productAbove100(productList);
  // mengambil 5 data dari data yang sudah di filter dg harga diatas 100 dann diurutkan dari harga tertinggi
  const highestPrice = [...filterProduct]
    .slice(0, 5)
    .sort((a, b) => b.price - a.price);

  //menghitung banyaknya produk yang memiliki nama Produk hemat
  const totalProdukHemat = updateProductName.filter(
    (product) => product.name === "Produk Hemat"
  ).length;

  // menghitung banyaknya produk yang memiliki nama Produk premium
  const totalProdukPremium = updateProductName.filter(
    (product) => product.name === "Produk Premium"
  ).length;

  const dashList = [
    {
      title: "Total Produk",
      path: "/product-list",
      total: productList.length,
      icon: <MdOutlineProductionQuantityLimits />,
    },
    {
      title: "Total Semua Harga",
      path: "/product-list",
      total: formatMoney(totalPrice),
      icon: <TbSum />,
    },
    {
      title: "Harga Rata-rata Semua Produk",
      path: "/product-list",
      total: formatMoney(averagePrice(productList)),
      icon: <TbBorderHorizontal />,
    },
    {
      title: "Harga Rata-rata Produk > $100",
      path: "/expensive-price",
      total: formatMoney(averagePrice(filterProduct)),
      icon: <TbBorderHorizontal />,
    },
  ];

  return (
    <div className="flex flex-col mx-6">
      <h1 className=" font-bold mt-6 text-lg">Dashboard</h1>
      <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4 gap-2  h-[30%] w-full mt-6">
        {/* menampilkan dashList */}
        {dashList?.map((item, index) => {
          const { title, path, total, icon } = item;
          return (
            <div
              className="text-black bg-[#2B2B36] h-28 rounded-md flex justify-between items-center px-3"
              key={index}
            >
              <div className="text-white">
                <h3 className="text-sm">{title}</h3>
                <p className="font-bold text-xl">{total}</p>
              </div>
              <Link
                to={path}
                className="flex justify-center items-center h-12 w-12 bg-[#54a8c7] hover:bg-blue-500 rounded-md text-black text-xl"
              >
                {icon}
              </Link>
            </div>
          );
        })}
      </div>
      <div className="flex flex-col lg:flex-row w-full gap-4">
        <div className="w-full lg:w-2/3">
          <h1 className=" font-semibold mt-6 mb-4 text-lg">Harga Termahal</h1>
          {/* menampilkan tabel dengan harga termahal */}
          <div className="hidden md:block">
            <TableComp data={highestPrice} currentPage={currentPage} />
          </div>
          <div className="block md:hidden">
            <TableSmallComp data={highestPrice} currentPage={currentPage} />
          </div>
        </div>
        {/* menampilkan info tambahan seperti jumlah per category, total produk hemat & total produk premium */}
        <div className="w-full lg:w-1/3">
          <h1 className=" font-semibold mt-6 text-lg mb-4">Info Tambahan</h1>
          <div className="bg-[#21222D] flex flex-col rounded-md p-4 mb-2 shadow-sm gap-4">
            <div className="flex flex-col gap-4">
              {data.map((produk, index) => (
                <div key={index} className="flex justify-between">
                  <p>Total Produk {produk.name}</p>{" "}
                  <p className="bg-gray-700 px-3 rounded-sm">{produk.value}</p>
                </div>
              ))}
            </div>
            <div className="flex justify-between">
              <p>Total Produk Hemat</p>
              <p className="bg-gray-700 px-3 rounded-sm">{totalProdukHemat}</p>
            </div>
            <div className="flex justify-between">
              <p>Total Produk Premium</p>{" "}
              <p className="bg-gray-700 px-3 rounded-sm">
                {totalProdukPremium}
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Dashboard;
