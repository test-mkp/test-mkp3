import useProduct from "@/hooks/useProduct";
import React from "react";
import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  LabelList,
  Cell,
} from "recharts";
const ProductChart = () => {
  const { calculateProductCategory, productList } = useProduct();
  const data = calculateProductCategory(productList);

  // fungsi untuk menentukan warna status berdasarkan nama statusnya
  const getColor = (status) => {
    switch (status) {
      case "Electronics":
        return "#FFA500";
      case "Home Appliances":
        return "#FF0000";
      case "Fashion":
        return "#008000";
      default:
        return "#8884d8";
    }
  };

  return (
    <div className=" my-20 mx-6">
      <h2 className="text-xl t font-bold mb-4">Produk Kategori Chart</h2>
      <div className="flex justify-center sm:pt-16">
        {/* menampilkan Bar chart */}
        <BarChart
          width={600}
          height={400}
          data={data}
          margin={{
            top: 5,
            right: 30,
            left: 20,
            bottom: 5,
          }}
        >
          <CartesianGrid strokeDasharray="2 2" />
          <XAxis dataKey="name" />
          <YAxis />
          <Tooltip />
          <Bar dataKey="value">
            <LabelList dataKey="value" position="top" />
            {data.map((entry, index) => (
              <Cell key={index} fill={getColor(entry.name)} />
            ))}
          </Bar>
        </BarChart>
      </div>
    </div>
  );
};

export default ProductChart;
