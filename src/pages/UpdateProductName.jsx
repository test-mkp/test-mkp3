import InputSearch from "@/components/common/InputSearch";
import Pagination from "@/components/common/Pagination";
import TableComp from "@/components/layout/TableComp";
import TableSmallComp from "@/components/layout/TableSmallComp";
import useProduct from "@/hooks/useProduct";
import React from "react";

const UpdateProductName = () => {
  const {
    currentPage,
    setCurrentPage,
    dataPerPage,
    updateProductName,
    setSearch,
    filteredData,
  } = useProduct();
  const { paginatedData, totalFilteredItems } = filteredData(updateProductName);

  return (
    <div className="mx-6 my-20">
      <h1 className="text-xl font-bold mb-4">Ganti Nama Produk</h1>
      {/* menampilkan InputSearch yaitu untuk fitur searching */}
      <div className="flex flex-col-reverse md:flex-row justify-between  pb-3">
        <InputSearch setSearch={setSearch} setCurrentPage={setCurrentPage} />
      </div>
      {/* menampilkan Table nama produk yang sudah diganti sesuai harga */}
      <div className="hidden md:block">
        <TableComp data={paginatedData} currentPage={currentPage} />
      </div>
      <div className="block md:hidden">
        <TableSmallComp data={paginatedData} currentPage={currentPage} />
      </div>
      {/* menampilkan fitur pagination */}
      <Pagination
        currentPage={currentPage}
        setCurrentPage={setCurrentPage}
        totalItems={totalFilteredItems}
        itemsPerPage={dataPerPage}
      />
    </div>
  );
};

export default UpdateProductName;
